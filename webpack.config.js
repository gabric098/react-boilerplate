const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: './source/client.js',
    output: {
        path: '/',
        filename: 'index.js',
    },
    devServer: {
        inline: true,
        contentBase: './public',
        port: 3000,
    },
    module: {
        loaders: [{
            test: /\.js$/,
            loader: 'babel-loader',
        }, {
            test: /\.js$/,
            include: [
                path.resolve(__dirname, 'source'),
            ],
            loader: 'eslint-loader',
            exclude: /node_modules/,
        }, {
            test: /\.css$/,
            loader: ExtractTextPlugin.extract(['css-loader']),
        },
        {
            test: /\.(scss|sass)$/,
            loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader']),
        }],
    },
    plugins: [
        new ExtractTextPlugin('app.css'),
    ],
};
