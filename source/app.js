import React from 'react';
import './appsource.scss';


export default class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = { click: 0 };
        this.onClick = this.onClick.bind(this);
        this.onReset = this.onReset.bind(this);
    }

    onClick() {
        this.setState({
            click: this.state.click + 1,
        });
    }

    onReset() {
        this.setState({
            click: 0,
        });
    }

    render() {
        const items = [];
        for (let i = 1; i <= this.state.click; i += 1) {
            items.push(<li key={i} className="appItem" >Item {i}</li>);
        }
        return (
            <div className="mainApp">
                <button onClick={this.onClick}>Add Item</button>
                <button onClick={this.onReset}>Reset</button>
                <ul>
                    {items}
                </ul>
            </div>
        );
    }
}
